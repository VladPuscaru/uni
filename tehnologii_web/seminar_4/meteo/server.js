`use strict`
const express = require('express');
const app = express();
const request = require('request-promise');
// prin proxy
// let proxiedRequest = request;
// request.defaults({
// 	proxy: "http://proxy.ase.ro:8008";
// });

const WEATHER_STATE = 'http://www.meteoromania.ro/wp-json/meteoapi/v2/starea-vremii';

app.locals.cache = null;

app.get('/weather', async (req, res) => {
	try {
		if (!app.locals.cache) {
			let response = await request(WEATHER_STATE);
			app.locals.cache = await JSON.parse(response);
		}


		console.log(app.locals.cache);

		let city = req.query.city;
		let weatherRecord = app.locals.cache.features.find((e) => {
			return e.properties.nume === city.toUpperCase();
		});

		if (weatherRecord) {
			res.status(200).json(weatherRecord);
		} else {
			res.status(404).json({message: 'Not found. Try another one'});
		}
	} catch (err) {
		console.warn(err);
		res.status(500).json({message: ':('});
	}
});


console.log('Server listening on port 8080...');

app.listen(8080);