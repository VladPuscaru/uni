let sampleArray = [1, 2, 3, 4, 5];

let map = (array, transform) => {
	let results = [];
	for (let e of array) {
		results.push(transform(e));
	}
	return results;
}

console.log(map(sampleArray, (x) => x * 2));