let fib = (order) => {
	switch (order) {
		case 0:
		case 1:
			return 1;
		default: 
			return fib(order - 1) + fib(order - 2);
	}
}

// console.log(fib(7));
if (process.argv.length <= 2) {
	console.log('usage: node fibonacci.js <order>');
} else {
	console.log(fib(parseInt(process.argv[2])));
}