let isPrime = (number) => {
	for (let i = 2; i <= Math.sqrt(number); i++) {
		if (!(number % i))
			return false;
	}
	return true;
}

if (process.argv.length <= 2) {
	console.log('usage: node primenumber.js <number>');
} else {
	console.log(`${parseInt(process.argv[2])} is ${isPrime(parseInt(process.argv[2])) ? 'prime' : 'not prime'}`);
}