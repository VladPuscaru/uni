let dictionary = ['the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog'];

let sampleText = `
	best
	read
	on
	windy
	nights
`;

let checkAcrostih = (text) => {
	let candidate = text.split('\n').map((e) => e.trim()).filter(
		(e) => e).map((e) => e[0]).join('');
	console.log(candidate);

	return dictionary.indexOf(candidate) !== -1;
}

console.log(checkAcrostih(sampleText));