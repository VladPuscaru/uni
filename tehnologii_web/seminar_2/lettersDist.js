let sampleString = 'the quick brown fox jumps over the lazy dog';

let getFDist = (text) => {
	result = {};
	for (let letter of text) {
		if (letter in result) {
			result[letter]++;
		} else {
			result[letter] = 1;
		}
	}

	for (let key in result) {
		result[key] /= text.length;
	}

	return result;
}

console.log(getFDist(sampleString));