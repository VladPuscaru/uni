function genCheckPrime() {
    let cache = [2,3];

    let checkAgainstCache = (n) => {
        for (let e of cache) {
            if (!(n % e)) {
                return false;
            }
        }

        return true;
    }

    return (n) => {
        let found = cache.indexOf(n) !== -1;
        if (found) {
            return true;
        } else {
            if (n < cache[cache.length - 1]) {
                return false;
            } else {
                for (let i = cache[cache.length - 1] + 2; i <= Math.sqrt(n); i += 2) {
                    if (checkAgainstCache(i)) {
                        cache.push(i);
                    }
                }
                return checkAgainstCache(n);
            }
        }
    }

}

let checkPrime = genCheckPrime();
console.log('88 is prime? ' + checkPrime(88));
console.log('93 is prime? ' + checkPrime(93));
console.log('121 is prime? ' + checkPrime(93));
console.log('113 is prime? ' + checkPrime(93));
console.log('125 is prime? ' + checkPrime(93));