const Robot = require('./robot.js');
const Weapon = require('./weapon.js');
const CombatRobot = require('./combatRobot.js');


let r1 = new Robot('Robotel');
r1.move();

let w1 = new Weapon('pew pew laser');
w1.fire();


let cr1 = new CombatRobot('Combat Robot');
cr1.addWeapon(w1);
cr1.fire();



Robot.prototype.fly = function() {
    console.log(`${this.name} is flying`);
}

cr1.fly();


let f0 = r1.fly;
f0.apply(cr1, []);
f0.call(r1);