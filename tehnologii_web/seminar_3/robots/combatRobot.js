const Robot = require('./robot.js')

class CombatRobot extends Robot {
    constructor(name) {
        super(name);
        this.weapons = [];
    }

    addWeapon(w) {
        this.weapons.push(w);
    }

    fire() {
        for (let w of this.weapons) {
            w.fire();
        }
    }
}

module.exports = CombatRobot