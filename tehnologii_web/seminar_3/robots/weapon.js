class Weapon {
    constructor(description) {
        this.description = description;
    }

    fire() {
        console.log(`${this.description} is firing`);
    }
}

module.exports = Weapon