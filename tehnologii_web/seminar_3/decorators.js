function testFunction(a, b, c) {
    for (let i = 0; i < 1000; i++) {
        console.log(a + b + c);
    }
}


function getTimeFunction(f) {
    return function(...args) {
        let before = Date.now();
        f(...args);
        console.log(`${f.name} took ${Date.now() - before} milliseconds to run`);
    }
}

let timedTestFunction = getTimeFunction(testFunction);
timedTestFunction(1, 2, 3);