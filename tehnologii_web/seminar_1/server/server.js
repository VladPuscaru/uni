const express = require('express')
const app = express()

app.use(express.static('public'))

app.get('/test', (req, res) => {
	res.send('this is a test')
})

app.listen(8080)