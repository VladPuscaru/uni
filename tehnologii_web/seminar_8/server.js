const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

let sequelize = new Sequelize('bike_db', 'root', 'root', {
  dialect : 'mysql'
});

let Bike = sequelize.define('bike', {
  gears : Sequelize.INTEGER,
  brand : Sequelize.STRING,
  weight : Sequelize.INTEGER
});

const app = express();
app.use(bodyParser.json());

app.post('/sync', async (req, res, next) => {
  try {
    await sequelize.sync({
      force : true // deleting and rewriting tables
    });
    res.status(201).json({
      message : 'Tables created'
    });
  } catch (err) {
    next(err);
  }
});

app.get('/bikes', async (req, res, next) => {
  try {
    let bikes = await Bike.findAll();
    res.status(200).json(bikes);
  } catch (err) {
    next(err);
  }
});

app.post('/bikes', async (req, res, next) => {
  try {
    if (req.body) {
      await Bike.create(req.body);
      res.status(200).json({
        message : 'Bike created!'
      });
    } else {
      res.status(401).json({
        message : 'Body is empty'
      });
    }
  } catch (err) {
    next(err);
  }
});

app.get('/bikes/:id', async (req, res, next) => {
  try {
    let bike = await Bike.findByPk(req.params.id);

    if (bike) {
      res.status(200).json({
        message : `Bike with id ${req.params.id} found!`,
        bike : bike
      });
    } else {
      res.status(404).json({
        message : `Bike with id ${req.params.id} not found.`
      });
    }
  } catch (err) {
    next(err);
  }
});

app.put('/bikes/:id', async (req, res, next) => {
  try {
    let bike = await Bike.findByPk(req.params.id);

    if (bike) {
      await bike.update(req.body);
      res.status(200).json({
        message : `Bike with id ${req.params.id} was updated!`
      });
    } else {
      res.status(404).json({
        message : `Bike width id ${req.params.id} not found :(`
      });
    }
  } catch (err) {
    next(err);
  }
});

app.delete('/bikes/:id', async (req, res, next) => {
  try {
    let bike = await Bike.findByPk(req.params.id);

    if (bike) {
      await bike.destroy();
      res.status(200).json({
        message : `Bike with id ${req.params.id} deleted!`
      });
    } else {
      res.status(404).json({
        message : `Bike with id ${req.params.id} not found :(`
      });
    }
  } catch (err) {
    next(err);
  }
});


// middleware for handling errors
app.use((err, req, res, next) => {
  console.warn(err);
  res.status(500).json({
    message : 'Server error. Sorry :('
  })
});


app.listen(8080, () => {
  console.log('Listening on port 8080...');
})